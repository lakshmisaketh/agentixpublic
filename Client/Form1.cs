﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleTCP;

namespace Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        SimpleTcpClient varClient;
        private void Form1_Load(object sender, EventArgs e)
        {
            varClient = new SimpleTcpClient();
            varClient.StringEncoder = Encoding.UTF8;
            varClient.DataReceived += Client_DataRecieved;
        }

        private void Client_DataRecieved(object sender, SimpleTCP.Message e)
        {
            textBox3.Invoke((MethodInvoker)delegate ()
            {
                textBox3.Text += e.MessageString;

            });
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            varClient.WriteLineAndGetReply(textBox1.Text, TimeSpan.FromSeconds(3));
        }

        private void bstStart_Click(object sender, EventArgs e)
        {
            bstStart.Enabled = false;
            varClient.Connect(txtHost.Text, Convert.ToInt32(textBox2.Text));

        }

        public void enable_startButton() => bstStart.Enabled = true;


    }
}
