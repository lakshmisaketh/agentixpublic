﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleTCP;
using Client;



namespace Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        SimpleTcpServer varServer;

        private void Form1_Load(object sender, EventArgs e)
        {
            varServer = new SimpleTcpServer();
            varServer.Delimiter = 0x13;//enter
            varServer.StringEncoder = Encoding.UTF8;
            varServer.DataReceived += Server_DataRecieved;
        }

        private void Server_DataRecieved(object sender, SimpleTCP.Message e)
        {
            txtStatus.Invoke((MethodInvoker)delegate ()
            {
                txtStatus.Text += e.MessageString;
                e.ReplyLine(string.Format("You Said :{0}", e.MessageString));
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtStatus.Text += "Server Starting ........";
            System.Net.IPAddress ip = System.Net.IPAddress.Parse(txtHost.Text);
            varServer.Start(ip, Convert.ToInt32(textBox2.Text));

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (varServer.IsStarted)
            {
                varServer.Stop();
                //Server.Form1 v = new Server.Form1();
                Client.Form1 c = new Client.Form1();
                c.enable_startButton();
            }

        }
    }
}
